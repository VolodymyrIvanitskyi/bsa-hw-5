import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectsComponent } from './components/project/projects/projects.component';
import { TasksComponent } from './components/task/tasks/tasks.component';
import { TeamsComponent } from './components/team/teams/teams.component';
import { UsersComponent } from './components/user/users/users.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpService } from './services/http.service';
import { CreateProjectComponent } from './components/project/create-project/create-project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateProjectComponent } from './components/project/update-project/update-project.component';
import { UaDatePipe } from './pipes/ua-date.pipe';
import { CreateTaskComponent } from './components/task/create-task/create-task.component';
import { UpdateTaskComponent } from './components/task/update-task/update-task.component';
import { CreateTeamComponent } from './components/team/create-team/create-team.component';
import { UpdateTeamComponent } from './components/team/update-team/update-team.component';
import { CreateUserComponent } from './components/user/create-user/create-user.component';
import { UpdateUserComponent } from './components/user/update-user/update-user.component';

@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    TasksComponent,
    TeamsComponent,
    UsersComponent,
    CreateProjectComponent,
    UpdateProjectComponent,
    UaDatePipe,
    CreateTaskComponent,
    UpdateTaskComponent,
    CreateTeamComponent,
    UpdateTeamComponent,
    CreateUserComponent,
    UpdateUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
