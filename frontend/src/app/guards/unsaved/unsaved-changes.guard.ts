import { compilePipeFromMetadata } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UnsavedChangesGuard implements CanDeactivate<ComponentCanDeactivate> {
  canDeactivate(
    component: ComponentCanDeactivate): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>
    {
      /*if(component.canDeactivate()){
        return true;
      }
      else{
        return confirm("Do you really want to leave the page? Changes will not be saved!");
      }*/
      return component.canDeactivate() ? component.canDeactivate() : true;
  }
}

export interface ComponentCanDeactivate{
  canDeactivate: () => boolean | Observable<boolean>;
}
