import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectsComponent } from './components/project/projects/projects.component';
import { TeamsComponent } from './components/team/teams/teams.component';
import { UsersComponent } from './components/user/users/users.component';
import { TasksComponent } from './components/task/tasks/tasks.component';
import { CreateProjectComponent } from './components/project/create-project/create-project.component';
import { UpdateProjectComponent } from './components/project/update-project/update-project.component';
import { UnsavedChangesGuard } from './guards/unsaved/unsaved-changes.guard';
import { CreateTaskComponent } from './components/task/create-task/create-task.component';
import { UpdateTaskComponent } from './components/task/update-task/update-task.component';
import { CreateTeamComponent } from './components/team/create-team/create-team.component';
import { UpdateTeamComponent } from './components/team/update-team/update-team.component';
import { CreateUserComponent } from './components/user/create-user/create-user.component';
import { UpdateUserComponent } from './components/user/update-user/update-user.component';

const routes: Routes = [
  {path:'projects',component:ProjectsComponent},
  {path:'teams', component:TeamsComponent},
  {path:'users', component:UsersComponent},
  {path:'tasks', component:TasksComponent},
  {path:'projects/add', component:CreateProjectComponent, canDeactivate:[UnsavedChangesGuard]},
  {path:'projects/update/:id',component:UpdateProjectComponent, canDeactivate:[UnsavedChangesGuard]},
  {path:'tasks/add', component:CreateTaskComponent,canDeactivate:[UnsavedChangesGuard]},
  {path:'tasks/update/:id', component:UpdateTaskComponent, canDeactivate:[UnsavedChangesGuard]},
  {path:'teams/add',component:CreateTeamComponent,canDeactivate:[UnsavedChangesGuard]},
  {path:'teams/update/:id',component:UpdateTeamComponent,canDeactivate:[UnsavedChangesGuard]},
  {path:'users/add',component:CreateUserComponent, canDeactivate:[UnsavedChangesGuard]},
  {path:'users/update/:id',component:UpdateUserComponent,canDeactivate:[UnsavedChangesGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
