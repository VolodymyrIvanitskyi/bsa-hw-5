import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css'],
  providers:[TaskService]
})
export class UpdateTaskComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private taskService:TaskService,
    private formBuilder:FormBuilder
  ) { 
    this.editTaskForm = this.formBuilder.group({
      id: '',
      name: '',
      description: '',
      projectId: '',
      performerId: '',
      state: '',
      createdAt: '',
      finishedAt:''
  });
  }

  public task:Task = {} as Task;
  public id:number = 0;

  editTaskForm : FormGroup = {} as FormGroup;

  private isDataSaved:boolean = false;

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];

    this.taskService.getTask(this.id)
    .subscribe(item=>{
      this.task = item.body !==null ? item.body : {} as Task;

      this.editTaskForm = new FormGroup({
        'name':new FormControl(this.task.name,[Validators.required, Validators.minLength(4)]),
        'description':new FormControl(this.task.description,[Validators.required, Validators.minLength(4)]),
        'projectId':new FormControl(this.task.projectId,[Validators.required]),
        'performerId':new FormControl(this.task.performerId,[Validators.required]),
        'state':new FormControl(this.task.state,[Validators.required]), //Add custom validator
        'createdAt':new FormControl(this.task.createdAt,[Validators.required]),
        'finishedAt':new FormControl(this.task.finishedAt)
      });
    });
  }


  public updateTask(_task:Task){
    _task.id = this.task.id;
    this.taskService.updateTask(_task)
    .subscribe(responce=>{
      alert("Task updated successfully");
        this.isDataSaved = true;
        this.router.navigate(['tasks']);
    });
  }

  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }
  
}
