import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css']
})
export class CreateTaskComponent implements OnInit, ComponentCanDeactivate {

  constructor(private router:Router,
    private taskService:TaskService,
    private formBuilder:FormBuilder) { 
      this.addTaskForm = this.formBuilder.group({
        id: '',
        name: '',
        description: '',
        projectId: '',
        performerId: '',
        state: '',
        createdAt: '',
        finishedAt:''
    });
    }

  public task:Task = {} as Task;
  public addTaskForm:FormGroup = {} as FormGroup;
  public isDataSaved:boolean = false;

  ngOnInit(): void {
    this.addTaskForm = new FormGroup({
      'name':new FormControl(this.task.name,[Validators.required, Validators.minLength(4)]),
      'description':new FormControl(this.task.description,[Validators.required, Validators.minLength(4)]),
      'projectId':new FormControl(this.task.projectId,[Validators.required]),
      'performerId':new FormControl(this.task.performerId,[Validators.required]),
      'state':new FormControl(this.task.state,[Validators.required]), //Add custom validator
      'createdAt':new FormControl(this.task.createdAt,[Validators.required]),
      'finishedAt':new FormControl(this.task.finishedAt)
    });
  }

  public addTask(_task:Task){
    this.taskService.createTask(_task)
    .subscribe(responce=>{
      alert("Task added successfully")
      this.isDataSaved = true;
      this.router.navigate(['tasks']);
    });
  }


  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }

}
