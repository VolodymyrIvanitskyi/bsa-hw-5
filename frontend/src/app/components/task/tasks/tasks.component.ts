import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
  providers:[TaskService]
})
export class TasksComponent implements OnInit {

  constructor(private taskService:TaskService) { }

  public tasks:Task[]=[];

  ngOnInit(): void {
    this.GetTasks();
  }

  public GetTasks(){
    this.taskService.getTasks()
    .subscribe(items=> this.tasks = items.body !== null ? items.body: []);
  }
  
  public deleteTask(id:number){
    if(confirm("You really want to delete this object???")){
      this.taskService.deleteTask(id).subscribe(responce=>
        {
          alert("Deleted successfully");
          this.GetTasks();
        });
    }
  }

}
