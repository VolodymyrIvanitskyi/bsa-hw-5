import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit,ComponentCanDeactivate {

  constructor(
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private formBuilder:FormBuilder,
    private userService:UserService) {
      this.editUserForm = this.formBuilder.group({
        id: '',
        firstName: '',
        lastName:'',
        email:'',
        teamId:'',
        registeredAt:'',
        birthDay:''
    });
     }

  public user:User = {} as User;
  public editUserForm:FormGroup = {} as FormGroup;
  public id:number=0;
  private isDataSaved:boolean = false;

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];

    this.userService.getUser(this.id)
    .subscribe(item=>{
      this.user = item.body !== null ? item.body : {} as User;
    });

    this.editUserForm = new FormGroup({
      'firstName':new FormControl(this.user.firstName,[Validators.required, Validators.minLength(2)]),
      'lastName':new FormControl(this.user.lastName,[Validators.required, Validators.minLength(2)]),
      'email':new FormControl(this.user.firstName,[Validators.required, Validators.email]),
      'teamId':new FormControl(this.user.teamId,[Validators.required]),
      'registeredAt':new FormControl(this.user.registeredAt,[Validators.required]),
      'birthDay':new FormControl(this.user.birthDay,[Validators.required]),
    });
  }

  public updateUser(_user:User){
    _user.id = this.user.id;
    this.userService.updateUser(_user)
    .subscribe(responce=>{
      alert("User updated successfully");
      this.isDataSaved=true;
      this.router.navigate(['users']);
    });
  }
  
  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }
}
