import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers:[UserService]
})
export class UsersComponent implements OnInit {

  public users : User[]=[];
  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.GetUsers();
  }

  public GetUsers(){
    this.userService.getUsers()
    .subscribe(
      items=>{
        this.users = items.body !== null ? items.body : [];
      }
    );
  }

  public deleteUser(id:number){
    if(confirm("You really want to delete this object???")){
      this.userService.deleteUser(id).subscribe(responce=>
        {
          alert("Deleted successfully");
          this.GetUsers();
        });
    }
  }

}
