import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private router:Router,
    private userService:UserService,
    private formBuilder:FormBuilder
    ) {
      this.addUserForm = this.formBuilder.group({
        id: '',
        firstName: '',
        lastName:'',
        email:'',
        teamId:'',
        registeredAt:'',
        birthDay:''
      });
   }

  public user:User = {} as User;
  public addUserForm:FormGroup = {} as FormGroup;
  private isDataSaved:boolean = false;

  ngOnInit(): void {
    this.addUserForm = new FormGroup({
      'firstName':new FormControl(this.user.firstName,[Validators.required, Validators.minLength(2)]),
      'lastName':new FormControl(this.user.lastName,[Validators.required, Validators.minLength(2)]),
      'email':new FormControl(this.user.firstName,[Validators.required, Validators.email]),
      'teamId':new FormControl(this.user.teamId,[Validators.required]),
      'registeredAt':new FormControl(this.user.registeredAt,[Validators.required]),
      'birthDay':new FormControl(this.user.birthDay,[Validators.required]),
    });
  }

  addUser(_user:User){
    this.userService.createUser(_user)
    .subscribe(responce=>{
      alert("user created successfully");
      this.isDataSaved = true;
      this.router.navigate(['users']);
    });
  }

  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }
}
