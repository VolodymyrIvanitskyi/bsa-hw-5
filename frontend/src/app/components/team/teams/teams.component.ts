import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css'],
  providers:[TeamService]
})
export class TeamsComponent implements OnInit {

  constructor(private teamService:TeamService) { }

  public teams:Team[]=[]
  ngOnInit(): void {
    this.GetTeams();
  }

  public GetTeams(){
    this.teamService.getTeams()
    .subscribe(items=>
      {
        this.teams = items.body !== null ? items.body : [];
      });
  }

  public deleteTeam(id:number){
    if(confirm("You really want to delete this object???")){
      this.teamService.deleteTeam(id).subscribe(responce=>
        {
          alert("Deleted successfully");
          this.GetTeams();
        });
    }
  }

}
