import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-update-team',
  templateUrl: './update-team.component.html',
  styleUrls: ['./update-team.component.css'],
  providers:[TeamService]
})
export class UpdateTeamComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private formBuilder:FormBuilder,
    private teamService:TeamService) {
      this.editTeamForm = this.formBuilder.group({
        id: '',
        name: '',
        createdAt: '',
    });
    }

  public team:Team = {} as Team;
  public id:number = 0;

  editTeamForm : FormGroup = {} as FormGroup;

  private isDataSaved:boolean = false;

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params['id'];

    this.teamService.getTeam(this.id)
    .subscribe(item=>{
      this.team = item.body !== null ? item.body : {} as Team;
    });

    this.editTeamForm = new FormGroup({
      'name':new FormControl(this.team.name,[Validators.required]),
      'createdAt':new FormControl(this.team.createdAt,[Validators.required])
    });
  }

  public updateTeam(_team:Team){
    _team.id = this.team.id;
    this.teamService.updateTeam(_team)
    .subscribe(responce=>{
      alert("Team updated successfully");
      this.isDataSaved=true;
      this.router.navigate(['teams']);
    });
  }

  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }
  
}
