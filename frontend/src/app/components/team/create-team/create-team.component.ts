import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css'],
  providers:[TeamService]
})
export class CreateTeamComponent implements OnInit,ComponentCanDeactivate {

  constructor(
    private teamService:TeamService,
    private router:Router
  ) { }

  public team:Team = {} as Team;
  public addTeamForm:FormGroup={}as FormGroup;
  public isDataSaved:boolean=false;
  ngOnInit(): void {
    this.addTeamForm = new FormGroup({
      'name':new FormControl(this.team.name,[Validators.required]),
      'createdAt':new FormControl(this.team.createdAt,[Validators.required])
    });
  }

  public addTeam(_team:Team){
    this.teamService.createTeam(_team)
    .subscribe(responce=>{
      alert("Team added successully");
      this.isDataSaved = true;
      this.router.navigate(['teams']);
    });
  }

  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }
}
