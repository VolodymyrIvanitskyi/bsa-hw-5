import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers:[ProjectService]
})
export class ProjectsComponent implements OnInit {

  constructor(private projectService:ProjectService) {
  }
  public projects:Project[]=[];

  ngOnInit(): void {
    this.GetProjects();
  }

  public deleteProject(id:number){
    if(confirm("You really want to delete this object???")){
      this.projectService.deleteProject(id).subscribe(responce=>
        {
          alert("Deleted successfully");
          this.GetProjects();
        });
    }
  }

  public GetProjects(){
    this.projectService.getProjects()
    .subscribe(items => 
      {
        this.projects = items.body !== null ? items.body : []; 
      }
      );
  }

}
