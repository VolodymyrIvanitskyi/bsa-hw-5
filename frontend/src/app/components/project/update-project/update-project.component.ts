import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';


@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.css'],
  providers:[ProjectService]
})
export class UpdateProjectComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private projectService:ProjectService,
    private activatedRoute:ActivatedRoute,
    private router:Router,
    private formBuilder:FormBuilder) { 

      this.editProjectForm = this.formBuilder.group({
        id: '',
        name: '',
        description: '',
        authorId: '',
        teamId: '',
        deadline: '',
        createdAt: ''
    });}

  public project:Project = {} as Project;
  public id:number = 0;

  editProjectForm : FormGroup = {} as FormGroup;

  private isDataSaved:boolean = false;

  ngOnInit(): void {

    this.id = this.activatedRoute.snapshot.params['id'];

    this.projectService.getProject(this.id)
    .subscribe(item=> {
      this.project = item.body !== null ? item.body : {} as Project;

      this.editProjectForm = new FormGroup({
          'name':new FormControl(this.project.name,[
            Validators.required,
            Validators.minLength(4)
          ]),
          'description':new FormControl(this.project.description,[
            Validators.required,
            Validators.minLength(4)
          ]),
          'authorId':new FormControl(this.project.authorId, Validators.required),
          'teamId':new FormControl(this.project.teamId, Validators.required),
          'deadline':new FormControl(new Date(this.project.deadline), Validators.required),
          'createdAt':new FormControl(new Date(this.project.createdAt), Validators.required)
      });

    });
  }


  public updateProject(_project:Project){

    _project.id = this.project.id;
    this.projectService.updateProject(_project)
    .subscribe(reponce=> 
      {
        alert("Project updated successfully");
        this.isDataSaved = true;
        this.router.navigate(['projects']);
      });
      
  }

  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }
}
