import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from 'src/app/guards/unsaved/unsaved-changes.guard';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css'],
  providers:[ProjectService]
})
export class CreateProjectComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private projectService:ProjectService,
    private router:Router,
    private formBuilder:FormBuilder) {
      this.projectForm = this.formBuilder.group({
        id: '',
        name: '',
        description: '',
        authorId: '',
        teamId: '',
        deadline: '',
        createdAt: ''
    });
     }

  @Input() project:Project = {} as Project;
  
  projectForm:FormGroup = {} as FormGroup;
  isDataSaved:boolean = false;

  ngOnInit(): void {
    this.projectForm = new FormGroup(
      {
          'name':new FormControl(this.project.name,[
            Validators.required,
            Validators.minLength(4)
          ]),
          'description':new FormControl(this.project.description,[
            Validators.required,
            Validators.minLength(4)
          ]),
          'authorId':new FormControl(this.project.authorId, Validators.required),
          'teamId':new FormControl(this.project.teamId, Validators.required),
          'deadline':new FormControl(this.project.deadline, Validators.required),
          'createdAt':new FormControl(this.project.createdAt, Validators.required)
      });
      
  }

  public addNewProject(newProject:Project){

    this.projectService.createProject(newProject)
    .subscribe(responce=>
      {
        alert("New project added successfully");
        this.isDataSaved = true;
        this.router.navigate(['/projects']);
      });
  }

  canDeactivate():boolean | Observable<boolean>{
    return this.isDataSaved ? true : confirm("Do you really want to leave the page? Changes will not be saved!");
  }

}
