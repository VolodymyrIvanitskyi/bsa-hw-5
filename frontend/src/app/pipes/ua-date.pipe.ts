import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'uaDate'
})
export class UaDatePipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): unknown {

    let options: Intl.DateTimeFormatOptions = {
      day: "numeric", month: "long", year: "numeric"
    };
    let date = new Date(value);

    return date.toLocaleDateString("uk-UA", options);
  }

}
