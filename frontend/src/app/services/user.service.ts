import { Injectable } from "@angular/core";
import { User } from "../models/user";
import { HttpService } from "./http.service";

@Injectable({ providedIn: 'root' })

export class UserService {
    public routePrefix = '/api/User';

    constructor(private httpService : HttpService) { }

    public getUser(id:number){
        return this.httpService.getFullRequest<User>(`${this.routePrefix}/` + id);
    }

    public getUsers() {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
    }

    public createUser(user:User){
        return this.httpService.postFullRequest<User>(`${this.routePrefix}`,user);
    }

    public updateUser(user:User){
        return this.httpService.putFullRequest<User>(`${this.routePrefix}`,user);
    }

    public deleteUser(id: number) {
        return this.httpService.deleteFullRequest<User>(`${this.routePrefix}/` + id);
    }
}
