import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Project } from "../models/project";
import { HttpService } from "./http.service";

@Injectable({ providedIn: 'root' })

export class ProjectService {
    public routePrefix = '/api/Project';

    constructor(private httpService : HttpService) { }

    public getProject(id:number){
        return this.httpService.getFullRequest<Project>(`${this.routePrefix}/` + id);
    }

    public getProjects(){
        return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
    }

    public createProject(project:Project){
        return this.httpService.postFullRequest<Project>(`${this.routePrefix}`,project);
    }

    public updateProject(project:Project){
        return this.httpService.putFullRequest<Project>(`${this.routePrefix}`,project);
    }

    public deleteProject(id: number) {
        return this.httpService.deleteFullRequest<Project>(`${this.routePrefix}/` + id);
    }
    
    
}
