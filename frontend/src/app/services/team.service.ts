import { Injectable } from "@angular/core";
import { Team } from "../models/team";
import { HttpService } from "./http.service";

@Injectable({ providedIn: 'root' })

export class TeamService {
    public routePrefix = '/api/Team';

    constructor(private httpService : HttpService) { }

    public getTeam(id:number){
        return this.httpService.getFullRequest<Team>(`${this.routePrefix}/` + id);
    }

    public getTeams() {
        return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
    }

    public createTeam(team:Team){
        return this.httpService.postFullRequest<Team>(`${this.routePrefix}`,team);
    }

    public updateTeam(team:Team){
        return this.httpService.putFullRequest<Team>(`${this.routePrefix}`,team);
    }

    public deleteTeam(id: number) {
        return this.httpService.deleteFullRequest<Team>(`${this.routePrefix}/` + id);
    }
}
