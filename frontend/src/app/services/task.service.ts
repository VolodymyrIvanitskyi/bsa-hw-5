import { Injectable } from "@angular/core";
import { Task } from "../models/task";
import { HttpService } from "./http.service";

@Injectable({ providedIn: 'root' })

export class TaskService {
    public routePrefix = '/api/Task';

    constructor(private httpService : HttpService) { }

    public getTask(id:number){
        return this.httpService.getFullRequest<Task>(`${this.routePrefix}/` + id);
    }

    public getTasks() {
        return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
    }

    public createTask(task:Task){
        return this.httpService.postFullRequest<Task>(`${this.routePrefix}`,task);
    }

    public updateTask(task:Task){
        return this.httpService.putFullRequest<Task>(`${this.routePrefix}`,task);
    }

    public deleteTask(id: number) {
        return this.httpService.deleteFullRequest<Task>(`${this.routePrefix}/` + id);
    }
}
