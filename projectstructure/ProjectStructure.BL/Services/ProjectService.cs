﻿using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System.Collections.Generic;
using System;
using ProjectStructure.BL.Exceptions;
using System.Threading.Tasks;

namespace ProjectStructure.BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Project> _projectRepository;

        public ProjectService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.ProjectRepository;
        }
        public async System.Threading.Tasks.Task CreateProject(ProjectDTO projectDTO)
        {
            if(await _projectRepository.Get(projectDTO.Id) is not null)
                throw new Exception($"Project with id {projectDTO.Id} already exists");

            var projectEntity = _mapper.Map<Project>(projectDTO);
            await _projectRepository.Create(projectEntity);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task DeleteProject(int projectId)
        {
            if(await _projectRepository.Get(projectId) is null)
                throw new NotFoundException("Project", projectId);
            await _projectRepository.Delete(projectId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            var allProjects = await _projectRepository.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(allProjects);
        }

        public async Task<ProjectDTO> GetProjectById(int projectId)
        {
            var projectEntity = await _projectRepository.Get(projectId);

            if (projectEntity is null)
                throw new NotFoundException("Project", projectId);

            return _mapper.Map<ProjectDTO>(projectEntity);
        }

        public async System.Threading.Tasks.Task UpdateProject(ProjectDTO projectDTO)
        {
            if (await _projectRepository.Get(projectDTO.Id) is null)
                throw new NotFoundException("Project", projectDTO.Id);

            var projectEntity = await _projectRepository.Get(projectDTO.Id);
            projectEntity = _mapper.Map(projectDTO,projectEntity);
            _projectRepository.Update(projectEntity);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}

