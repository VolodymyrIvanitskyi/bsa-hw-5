﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;
using ProjectStructure.Common.DTO.QueryDTO;
using System.Threading.Tasks;

namespace ProjectStructure.BL.Interfaces
{
    public interface IQueryService
    {
        public Task<IEnumerable<TaskDTO>> GetTasksForUser(int userId);
        public Task<Dictionary<int, int>> GetCountTasksByUser(int userId);
        public Task<IEnumerable<TaskDTO>> GetFinishedTasksForUser(int userId);
        public Task<IEnumerable<OlderTeamDTO>> GetTeamsWhenMembersOlderThan10Years();
        public Task<IEnumerable<UserTasksDTO>> GetUsersAlphabetically();
        public Task<DataFromUserDTO> GetDatafromUser(int id);
        public Task<IEnumerable<DataFromProjectDTO>> GetDataFromProject();


    }
}
