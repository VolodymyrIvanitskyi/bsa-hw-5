﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 3, 31, 5, 29, 28, 374, DateTimeKind.Local).AddTicks(504), "Durgan Group" },
                    { 2, new DateTime(2021, 2, 21, 17, 47, 30, 379, DateTimeKind.Local).AddTicks(7852), "Kassulke LLC" },
                    { 3, new DateTime(2020, 8, 28, 11, 18, 46, 416, DateTimeKind.Local).AddTicks(342), "Harris LLC" },
                    { 4, new DateTime(2021, 4, 3, 12, 58, 33, 17, DateTimeKind.Local).AddTicks(8179), "Mitchell Inc" },
                    { 5, new DateTime(2019, 10, 5, 10, 57, 2, 842, DateTimeKind.Local).AddTicks(7653), "Smitham Group" },
                    { 6, new DateTime(2020, 10, 31, 7, 5, 15, 107, DateTimeKind.Local).AddTicks(6578), "Kutch - Roberts" },
                    { 7, new DateTime(2021, 7, 17, 4, 34, 55, 91, DateTimeKind.Local).AddTicks(7082), "Parisian Group" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(1992, 9, 27, 23, 27, 3, 523, DateTimeKind.Local).AddTicks(6015), "Theresa_Gottlieb66@yahoo.com", "Theresa", "Gottlieb", new DateTime(2019, 12, 4, 18, 52, 4, 372, DateTimeKind.Local).AddTicks(7619), 1 },
                    { 2, new DateTime(2007, 1, 1, 7, 10, 18, 898, DateTimeKind.Local).AddTicks(8690), "Brandy.Witting@gmail.com", "Brandy", "Witting", new DateTime(2019, 1, 10, 4, 51, 56, 714, DateTimeKind.Local).AddTicks(8896), 1 },
                    { 3, new DateTime(1980, 2, 20, 18, 32, 12, 635, DateTimeKind.Local).AddTicks(8667), "Theresa82@hotmail.com", "Theresa", "Ebert", new DateTime(2021, 5, 14, 5, 37, 44, 448, DateTimeKind.Local).AddTicks(6766), 1 },
                    { 4, new DateTime(1954, 4, 9, 8, 4, 50, 470, DateTimeKind.Local).AddTicks(9098), "Alfredo_Simonis@yahoo.com", "Alfredo", "Simonis", new DateTime(2020, 10, 14, 19, 40, 52, 277, DateTimeKind.Local).AddTicks(2028), 2 },
                    { 5, new DateTime(2009, 5, 12, 5, 9, 16, 237, DateTimeKind.Local).AddTicks(3321), "Joanna25@hotmail.com", "Joanna", "Botsford", new DateTime(2021, 5, 14, 21, 2, 44, 99, DateTimeKind.Local).AddTicks(5821), 2 },
                    { 6, new DateTime(1962, 2, 26, 10, 8, 59, 71, DateTimeKind.Local).AddTicks(1820), "Ann.Langworth@hotmail.com", "Ann", "Langworth", new DateTime(2019, 8, 9, 8, 47, 42, 369, DateTimeKind.Local).AddTicks(9036), 2 },
                    { 7, new DateTime(1981, 1, 10, 13, 38, 30, 529, DateTimeKind.Local).AddTicks(222), "Christie.Gusikowski@hotmail.com", "Christie", "Gusikowski", new DateTime(2021, 10, 7, 10, 41, 43, 246, DateTimeKind.Local).AddTicks(699), 3 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2021, 8, 25, 20, 49, 50, 451, DateTimeKind.Local).AddTicks(8054), new DateTime(2021, 9, 12, 22, 17, 47, 233, DateTimeKind.Local).AddTicks(5223), "Et doloribus et temporibus.", "backing up Handcrafted Fresh Shoes challenge", 1 },
                    { 2, 2, new DateTime(2021, 1, 30, 16, 38, 53, 883, DateTimeKind.Local).AddTicks(8745), new DateTime(2021, 7, 24, 15, 7, 31, 935, DateTimeKind.Local).AddTicks(7846), "Non voluptatem voluptas libero.", "Village", 1 },
                    { 3, 3, new DateTime(2020, 3, 15, 22, 33, 15, 673, DateTimeKind.Local).AddTicks(1141), new DateTime(2021, 7, 21, 11, 32, 51, 335, DateTimeKind.Local).AddTicks(4654), "Quia et tempora hic pariatur voluptatem doloribus sunt.", "Dam", 1 },
                    { 4, 4, new DateTime(2021, 7, 3, 6, 39, 1, 997, DateTimeKind.Local).AddTicks(8679), new DateTime(2022, 6, 25, 14, 25, 0, 711, DateTimeKind.Local).AddTicks(1264), "Soluta non sed assumenda.", "iterate project", 2 },
                    { 5, 5, new DateTime(2021, 1, 15, 21, 47, 35, 933, DateTimeKind.Local).AddTicks(5401), new DateTime(2022, 4, 14, 5, 53, 20, 800, DateTimeKind.Local).AddTicks(3038), "Voluptatibus error ut id libero quam natus molestias natus.", "Libyan Dinar Netherlands Antilles", 2 },
                    { 6, 6, new DateTime(2022, 1, 22, 18, 51, 40, 196, DateTimeKind.Local).AddTicks(5166), new DateTime(2022, 9, 23, 6, 11, 42, 199, DateTimeKind.Local).AddTicks(4133), "Quas fuga qui eaque et corporis.", "New Jersey capacitor program", 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 2, new DateTime(2022, 1, 15, 17, 6, 52, 60, DateTimeKind.Local).AddTicks(666), "Eum a eum.", null, "product Direct utilize", 2, 2, 2 },
                    { 4, new DateTime(2022, 1, 19, 2, 58, 34, 804, DateTimeKind.Local).AddTicks(5103), "Delectus quibusdam id quia iure neque maiores molestias sed aut.", null, "withdrawal contextually-based", 4, 2, 2 },
                    { 5, new DateTime(2021, 12, 15, 8, 3, 48, 73, DateTimeKind.Local).AddTicks(2466), "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", null, "mobile Organized", 3, 2, 2 },
                    { 6, new DateTime(2021, 10, 21, 17, 56, 53, 811, DateTimeKind.Local).AddTicks(7818), "Reiciendis iusto rerum non et aut eaque.", new DateTime(2021, 12, 21, 16, 56, 53, 811, DateTimeKind.Local).AddTicks(7818), "world-class Circles", 2, 2, 2 },
                    { 3, new DateTime(2021, 8, 16, 9, 13, 44, 577, DateTimeKind.Local).AddTicks(3845), "Sint voluptatem quas.", new DateTime(2021, 9, 9, 9, 34, 47, 460, DateTimeKind.Local).AddTicks(2160), "bypass", 3, 3, 2 },
                    { 7, new DateTime(2021, 11, 10, 18, 21, 12, 88, DateTimeKind.Local).AddTicks(6153), "Et rerum ad.", new DateTime(2021, 12, 10, 18, 21, 12, 88, DateTimeKind.Local).AddTicks(6153), "Automotive & Tools transitional bifurcated", 4, 3, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
