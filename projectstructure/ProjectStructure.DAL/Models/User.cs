﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.DAL.Models
{
    public class User
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        [Required]
        [MaxLength(20)]
        public string FirstName { get; set; }
        [MaxLength(20)]
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
