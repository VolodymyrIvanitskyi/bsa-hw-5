﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class TeamRepository : IRepository<Team>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public TeamRepository(ProjectDBContext dBContext)
		{
			_dBContext = dBContext;
		}
		public async System.Threading.Tasks.Task Create(Team team)
        {
            await _dBContext.Teams.AddAsync(team);
        }

		public async System.Threading.Tasks.Task Delete(int id)
		{
			var team = await _dBContext.Teams.FindAsync(id);
			_dBContext.Teams.Remove(team);
		}

		public async Task<Team> Get(int id)
		{
			return await _dBContext.Teams.FindAsync(id);
		}

		public async Task<IEnumerable<Team>> GetAll()
		{
			return await _dBContext.Teams.ToListAsync();
		}

		public void Update(Team team)
		{
			_dBContext.Entry(team).State = EntityState.Modified;
		}

		public void Save()
		{
			_dBContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
