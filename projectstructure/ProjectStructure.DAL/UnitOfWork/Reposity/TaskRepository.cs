﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class TaskRepository : IRepository<Task>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public TaskRepository(ProjectDBContext dBContext)
		{
			_dBContext = dBContext;
		}
		public async System.Threading.Tasks.Task Create(Task task)
		{
			await _dBContext.Tasks.AddAsync(task);
		}

		public async System.Threading.Tasks.Task Delete(int id)
		{
			var task = await _dBContext.Tasks.FindAsync(id);
			_dBContext.Tasks.Remove(task);
		}

		public async System.Threading.Tasks.Task<Task> Get(int id)
		{
			return await _dBContext.Tasks.FindAsync(id);
		}

		public async System.Threading.Tasks.Task<IEnumerable<Task>> GetAll()
		{
			return await _dBContext.Tasks.ToListAsync();
		}

		public void Update(Task task)
		{
			_dBContext.Entry(task).State = EntityState.Modified;
		}

		public void Save()
		{
			_dBContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
