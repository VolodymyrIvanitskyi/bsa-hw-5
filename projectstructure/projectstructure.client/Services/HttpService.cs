﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Services
{
    public class HttpService
    {
        private static readonly HttpClient _client;

        static HttpService()
        {
            _client = new HttpClient();
        }

        public async Task<List<T>> GetEntities<T>(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(Settings.ServerPath + path);;

            if (response.IsSuccessStatusCode)
            {
                string responceBody = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<List<T>>(responceBody);
                return result;
            }
            else
            {
                string emessage = response.Content.ReadAsStringAsync().Result;
                throw new Exception(emessage);
            }
        }

        public async Task<T> GetEntity<T>(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(Settings.ServerPath + path);

            if (response.IsSuccessStatusCode)
            {
                string responceBody = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<T>(responceBody);
                return result;
            }
            else
            {
                string emessage = response.Content.ReadAsStringAsync().Result;
                throw new Exception(emessage);
            }
        }

        public async Task<bool> PutEntity<T>(string path, T entity)
        {
            string json = JsonConvert.SerializeObject(entity);
            StringContent data = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PutAsync($"{Settings.ServerPath}{path}", data);
            
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteEntity<T>(string path, int id)
        {
            HttpResponseMessage responce = await _client.DeleteAsync($"{Settings.ServerPath}{path}/{id}");

            return responce.IsSuccessStatusCode;
        }

        public async Task<bool> PostEntity<T>(string path, T entity)
        {
            string json = JsonConvert.SerializeObject(entity);
            StringContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage responce = await _client.PostAsync($"{Settings.ServerPath}{path}", stringContent);

            return responce.IsSuccessStatusCode;
        }
    }
}
